from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from django.http import HttpResponse
from django.contrib.sessions.models import Session
from django.views import View
from .models import Buyer
from .forms import *
from django.db.models import Sum
from django.db.models import F
# Create your views here.


class ThisView(View):
    def get(self,request):
        form = ProductRegistraion()

        # buyer = Buyer.objects.get(user = request.session.get('user_id'))
        # buyercarts = Cart.objects.filter(customer=buyer.id)
        # price = ((request.POST.get('price'))*(request.POST.get('quantity')))
        # total = sum(price)
        # print(total)
        # context = {
        #     'form': form,
        #     'cart': buyercarts,
        #     'amount': total
        # }
        buyer = Buyer.objects.get(user = request.session.get('user_id'))
        buyercarts = Cart.objects.filter(customer=buyer.id)
        # product = Cart.objects.filter(buyercarts=buyercarts)
        cartproduct = request.POST.get('cartProduct')
        context = {
            'form': form,
            'cart': buyercarts,
            'product' : buyercarts
        }
        return render(request,'home.html',context) 
    def post(self, request):
        print(request.POST)
        buyer = Buyer.objects.get(user = request.session.get('user_id'))
        cartproduct = Cart.objects.filter(customer=buyer, cartProduct = request.POST.get('cartProduct'))
        # cartproduct = request.POST.get('cartProduct')
        if cartproduct:
            cartproduct[0].quantity += int(request.POST.get('quantity'))
            cartproduct[0].save()
        else:
            sf = ProductCart({'cartProduct':request.POST.get('cartProduct'), 'quantity':request.POST.get('quantity') , "customer": buyer.id})
            if sf.is_valid():
                sf.save()
            print(sf.errors)
        return redirect('home')
def  signup(request):
    if request.method=='POST':
        username=request.POST.get('username')
        email=request.POST.get('email')
        pass1=request.POST.get('password1')
        pass2=request.POST.get('password2')

        if pass1!=pass2:
            return HttpResponse("password are not matched")
        else:
            print(request.POST)
            user = User.objects.create_user(username = username ,email = email, password=pass1)
            user.save()
    if request.session.get('is_logged'):
        return redirect('home')
    
    return render(request,'signup.html')
def userlogin(request):
    if request.method=='POST':
        name=request.POST.get('username')
        pass1=request.POST.get('pass')

        user = authenticate(username=name, password=pass1)
        if user is not None:
            login(request,user)
            request.session['is_logged'] = True
            request.session['user_id'] = user.id
            return redirect('home')
        else:
            return redirect('signup')
            return HttpResponse("username and password is incorrect")
    if request.session.get('is_logged'):
        return redirect('home')
    
    return render(request,'login.html')

def home(request):
    if request.session.has_key('is_logged'):
        return render(request,'home.html')
    return redirect('login')   

def userlogout(request):
    logout(request)
    return redirect('login')

def checkout(request):
    buyer = Buyer.objects.get(user = request.session.get('user_id'))
    buyercarts = Cart.objects.filter(customer=buyer.id)
    # product = Cart.objects.filter(buyercarts=buyercarts)
    # cartproduct = request.POST.get('cartProduct')
    total = buyercarts.aggregate(total_price=Sum(F('cartProduct__price') * F('quantity')))
    print(total.get('total_price'))
    context = {
        'cart': buyercarts,
        'product' : buyercarts,
        'total' : total.get('total_price')
    }
    return render(request,'checkout.html',context)

def payment(request):
    buyer = Buyer.objects.get(user = request.session.get('user_id'))
    buyercarts = Cart.objects.filter(customer=buyer.id).delete()
    print(buyercarts)
    return redirect('home')