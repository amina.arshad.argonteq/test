from django.forms import ModelForm
from .models import User , Owner , Buyer , Category , Product  , Cart


class ProductRegistraion(ModelForm):

    class Meta:
        model = Cart
        fields = ['cartProduct' , 'quantity']

class ProductCart(ModelForm):

    class Meta:
        model = Cart
        fields = '__all__'

        