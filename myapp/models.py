from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Owner(models.Model):
    name = models.CharField(max_length=20)
    age= models.IntegerField()
    address = models.CharField(max_length=30)
    phone_number = models.CharField(max_length=13)
    user = models.OneToOneField(User,  on_delete=models.CASCADE,null=True, blank=True)
    def __str__(self) -> str:
        return self.name

class Buyer(models.Model):
    name = models.CharField(max_length=20)
    age= models.IntegerField()
    address = models.CharField(max_length=30)
    phone_number = models.CharField(max_length=13)
    user = models.OneToOneField(User,  on_delete=models.CASCADE,null=True, blank=True)

    def __str__(self) -> str:
        return self.name
class Category(models.Model):
    DOC_CHOICES = [
            ('toys', 'toys'),
            ('watches', 'watches'),
            ('clothes', 'clothes'),
            ('shoes', 'shoes'),
            ('hats', 'hats'),
            ('jawallary', 'jawallary'),
            ('bags', 'bags'),
            ('coats', 'coats'),
            ('jackets', 'jackets'),
        ]    
    category_type = models.CharField(choices=DOC_CHOICES , max_length=50)
    name = models.CharField(max_length=150)

class Product(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    price = models.DecimalField(max_digits=200, decimal_places=2,null=True, blank=True)
    product_category = models.ForeignKey(Category,  on_delete=models.CASCADE,null=True, blank=True)

    def __str__(self) -> str:
        return self.name

class Cart(models.Model):
    customer = models.ForeignKey(Buyer,  on_delete=models.CASCADE,null=True, blank=True)
    cartProduct = models.ForeignKey(Product , on_delete=models.CASCADE ,null=True , blank=True)
    quantity = models.PositiveBigIntegerField()

# class BuyItems(models.Model):
#     item_detail = models.OneToOneField(Cart,  on_delete=models.CASCADE,null=True, blank=True)
#     # date_added=models.DateTimeField( auto_now_add=True,null=True,blank=True)