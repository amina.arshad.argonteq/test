from django.contrib import admin
from .models import Owner , Buyer , Category , Product  , Cart
# Register your models here.

admin.site.register(Owner)
admin.site.register(Buyer)
admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Cart)